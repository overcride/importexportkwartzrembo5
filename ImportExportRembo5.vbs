'Version 2.4
'Cr�� par Emmanuel LANDRAIN
'contact@landrain-emmanuel.fr

'Test l'existence d'un fichier
Function FileStatus(filePath)
	Dim fsoFileStatus
	Set fsoFileStatus = CreateObject("Scripting.FileSystemObject")
	If(fsoFileStatus.FileExists(filePath)) Then
		FileStatus = true
	Else
		FileStatus = false
	End If
End function

'Test l'existence d'un r�pertoire
Function FolderStatus(folderPath)
	Dim fsoFolderStatus
	Set fsoFolderStatus = CreateObject("Scripting.FileSystemObject")
	If(fsoFolderStatus.FolderExists(folderPath)) Then
		FolderStatus = true
	Else
		FolderStatus = false
	End If
End function

'R�cup le chemin d'un fichier
Function GetFileDlgEx(sIniDir,sFilter,sTitle) 
	Set oDlg = CreateObject("WScript.Shell").Exec("mshta.exe ""about:<object id=d classid=clsid:3050f4e1-98b5-11cf-bb82-00aa00bdce0b></object><script>moveTo(0,-9999);eval(new ActiveXObject('Scripting.FileSystemObject').GetStandardStream(0).Read("&Len(sIniDir)+Len(sFilter)+Len(sTitle)+41&"));function window.onload(){var p=/[^\0]*/;new ActiveXObject('Scripting.FileSystemObject').GetStandardStream(1).Write(p.exec(d.object.openfiledlg(iniDir,null,filter,title)));close();}</script><hta:application showintaskbar=no />""") 
	oDlg.StdIn.Write "var iniDir='" & sIniDir & "';var filter='" & sFilter & "';var title='" & sTitle & "';" 
	GetFileDlgEx = oDlg.StdOut.ReadAll 
End Function

'Set du chemin actuel du script
Dim currentPath
currentPath=Replace(WScript.ScriptFullName, WScript.ScriptName, "")


Dim rbagentPath, rbagentPathTest
rbagentPath = "C:\TEMP\rbagent.exe"
rbagentPathTest = FileStatus(rbagentPath)
If Not rbagentPathTest Then 'Si rbagent n'est pas dans le dossier "temp"
	rbagentPath = "H:\Outils Rembo\Rembo5\rbagent.exe"
	rbagentPathTest = FileStatus(rbagentPath)
	If Not rbagentPathTest Then 'Si rbagent n'est pas dans le dossier "Outils rembo" du lecteur H	
		MsgBox "Le fichier rbagent.exe n'a pas �t� trouv�.",vbExclamation,"rbagent introuvable"
		'Set manuel du fichier rbagent
		sIniDirRbagentPath = currentPath&"rbagent.exe" ' J'initialise le chemin par d�fault pour la recherche du fichier	
		sFilterRbagentPath = "Fichier rbagent.exe|rbagent.exe|" ' Je n'autorise que les fichiers rbagent.exe
		sTitleRbagentPath = "Ou se trouve votre fichier rbagent.exe?" ' Je set le nom de la fenetre
		rbagentPath = GetFileDlgEx(Replace(sIniDirRbagentPath,"\","\\"),sFilterRbagentPath,sTitleRbagentPath)
		If NOT rbagentPath <> "" Then 'Aucun chemin n'est indiqu�
			MsgBox "Vous n'avez pas selectionner le fichier rbagent.exe merci de relancer le script",vbExclamation,"rbagent introuvable"
			WScript.Quit
		End If
	End if
End If

Dim rembopassPath, rembopassPathTest
rembopassPath = "C:\TEMP\rembopass.txt"
rembopassPathTest = FileStatus(rembopassPath)
If Not rembopassPathTest Then 'Si rembopass n'est pas dans le dossier "temp"
	rembopassPath = "H:\Outils Rembo\Rembo5\rembopass.txt"
	rembopassPathTest = FileStatus(rembopassPath)
	If Not rembopassPathTest Then 'Si rembopass n'est pas dans le dossier "Outils rembo" du lecteur H
		MsgBox "Le fichier rembopass.txt n'a pas �t� trouv�.",vbExclamation,"rembopass introuvable"
		'Set manuel du fichier rembopass
		sIniDirRembopassPath = currentPath&"rembopass.txt" ' J'initialise le chemin par d�fault pour la recherche du fichier	
		sFilterRembopassPath = "Fichier rembopass.txt|rembopass.txt|" ' Je n'autorise que les fichiers rembopass.txt
		sTitleRembopassPath = "Ou se trouve votre fichier rembopass.txt?" ' Je set le nom de la fenetre
		rembopassPath = GetFileDlgEx(Replace(sIniDirRembopassPath,"\","\\"),sFilterRembopassPath,sTitleRembopassPath)
		If NOT rembopassPath <> "" Then 'Aucun chemin n'est indiqu�
			MsgBox "Vous n'avez pas selectionner le fichier rembopass.txt merci de relancer le script",vbExclamation,"rembopass introuvable"
			WScript.Quit
		End If
	End If
End If

If rbagentPath <> "C:\TEMP\rbagent.exe" AND rembopassPath <> "C:\TEMP\rembopass.txt" Then 'Si aucun des deux fichier n'est dans le dossier temp.
	Dim tempFolderPath, tempFolderPathTest
	tempFolderPath = "C:\TEMP"
	tempFolderPathTest = FolderStatus(tempFolderPath) 
	if NOT(tempFolderPathTest) Then 'Si le dossier temp n'existe pas le cr�e
		Set fsoCreateFolder = CreateObject("Scripting.FileSystemObject")
		fsoCreateFolder.CreateFolder("C:\TEMP") 'Je cr�e le dossier TEMP
	End If
	'Je copie les deux fichier dans le dossier temp
	Set fsoCopyFile = CreateObject("Scripting.FileSystemObject") 
	fsoCopyFile.CopyFile rbagentPath, "C:\TEMP\"
	fsoCopyFile.CopyFile rembopassPath, "C:\TEMP\"
ElseIf rbagentPath <> "C:\TEMP\rbagent.exe" Then 'Si rbagent n'est pas dans temp je le copie
	Set fsoCopyFile = CreateObject("Scripting.FileSystemObject") 
	fsoCopyFile.CopyFile rbagentPath, "C:\TEMP\"
ElseIf rembopassPath <> "C:\TEMP\rembopass.txt" Then 'Si rembopass n'est pas dans temp je le copie
	Set fsoCopyFile = CreateObject("Scripting.FileSystemObject")
	fsoCopyFile.CopyFile rembopassPath, "C:\TEMP\"
End If

'Je set le mot de passe rembo dans son fichier 
Dim fsoMdpRembo, text, rembopassLocation, mdpRembo  
Set fsoMdpRembo = CreateObject("Scripting.FileSystemObject")
rembopassLocation = "C:\TEMP\rembopass.txt"

Set text = fsoMdpRembo.OpenTextFile(rembopassLocation, 1)  
mdpRembo = text.ReadAll  
text.Close

'Je set l'IP du serveur
Dim ipServeur
ipServeur=inputBox("Quel est l'ip du serveur ?","IP du serveur", "172.16.0.253")

'Je set le chemin des logs
Dim logPath
logPath = """c:\TEMP\Rembo.log"""

'Je set le chemin du rbagent
rbagentPath = """c:\TEMP\rbagent.exe"""

'Je check si je veux faire un import ou un export
Dim importExport, imagePath
importExport=inputBox("1 = Import vers le serveur"&Chr(13)&"2 = Export vers le PC","Import ou Export", "1")
If importExport = 1 Then '*Import
	Dim command, sIniDirImagePath, sFilterImagePath, sTitleImagePath
	'Je set l'image d'import
	sIniDirImagePath = currentPath&"*.*rad" ' J'initialise le chemin par d�fault pour la recherche du fichier	
	sFilterImagePath = "Fichier REMBO(*.rad)|*.rad|" ' Je n'autorise que les fichiers .rad
	sTitleImagePath = "Selection de l image" ' Je set le nom de la fenetre
	imagePath = GetFileDlgEx(Replace(sIniDirImagePath,"\","\\"),sFilterImagePath,sTitleImagePath)	
	If NOT imagePath <> "" Then 'Si aucune image n'est s�lectionn� 
		MsgBox "Vous n'avez pas selectionn� l'image � importer merci de relancer le script",vbExclamation,"Image non selectionn�e"
		WScript.Quit
	End If	
	command = rbagentPath&" -d -v 4 -l "&logPath&" -s "&ipServeur&":"&mdpRembo&" kwartz-radput """&imagePath&""""
ElseIf importExport = 2 Then 'Export
	Dim imageName, newImageName
	imagePath = inputBox("Dans quel repertoire voulez-vous enregistrer l'image ?","Chemin d'enregistrement de l'image", "c:\TEMP")
	imageName = inputBox("Quel est le nom de l'image � exporter ?", "nomImage")
	newImageName = inputBox("Quel nom voulez-vous � l'image export� ?","Nom du nouvreau fichier", imageName)
	command = rbagentPath&" -d -v 4 -l "&logPath&" -s "&ipServeur&":"&mdpRembo&" kwartz-radget """&imagePath&"\"&newImageName&".rad"" "&imageName
Else 'Autre
	MsgBox "Erreur de saisie, merci de relancer le script",vbExclamation,"Erreur de saisie"
	WScript.Quit
End If

'Set WshShell = Wscript.CreateObject("Wscript.Shell")
'Wshshell.run command,1,True
MsgBox command